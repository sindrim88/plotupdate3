#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

# num1 = starting time, num2 = ending time.
def twoPlot(num1, num2, sid, nameToSave, size, title):
    import cparser
    import logging
    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import tickLabels.functions as ft
    import GetComponents.SeisGpsGasHytro as multi
    import GetComponents.get_Data as data
    import GetComponents.plotComponents  as plc 
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    
    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    end_date =  plc.getEndDate(num2)
    
    StaPars = cparser.Parser()
    
    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    
    constr = plc.getConstr(sid)
    
    GPS = data.getGPS(sid, dt_end, dt_start)
    Seis = data.getDataBase(dt_start, dt_end, sid)
    
    fig = ft.fourFrame(Ylabel=None, Title=None, depth = None, figNum = 2)
    fig = ft.tstwofigTickLabelsFourFrame(fig,period=(dt_end-dt_start),period2 = None,  figNum = 2, Depth = None)
    
    graphTitle = plc.getTitle(sid, title) 
    supTitle="Jardskjalftamaelingar vid " + graphTitle
    fig.suptitle(supTitle, fontsize=40, verticalalignment="center", x = 0.813, y = 0.935)
    
    # setting upp the axis stuff
    seis_axis = fig.axes[0] 
    seis_maxes = [seis_axis, seis_axis.twinx()]
    
    gps_axis = fig.axes[1]
    gps_maxes = [gps_axis]
    
    fig.subplots_adjust(right=1.5)
    
    seis_axis.set_xlim([dt_start,dt_end])
    gps_axis.set_xlim([dt_start,dt_end])
    
    # create the plot, pass axis as arguments to have more control over 
    # the order of things to draw, maybe if we wnt multiple axis later
    multi.plotSeis(Seis,seis_axis, seis_maxes[0], seis_maxes[1], sid, constr)
    multi.plotGPS(GPS, gps_axis, gps_maxes[0], sid, constr)
    
    plc.annotateBigSized(Seis, start, dt_end, seis_maxes[0], size)
    
    # write to image file
    home = Path.home()
    relpath = "multiplot/figures/"
    filebase = sid + "_Two_" + nameToSave
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"
    print(filebase + ".png created")
    tplt.saveFig(filename, "png", fig)
    del fig


    