#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function                 

# num1 = starting time, num2 = ending time
def fourPlot(num1, num2, sid):
    import cparser
    import logging
    from pathlib import Path
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    import timesmatplt.timesmatplt as tplt  # plot GPS data?
    import tickLabels.functions as ft
    import GetComponents.SeisGpsGasHytro as multi
    import GetComponents.get_Data as data
    import GetComponents.plotComponents  as plc   
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format

    start = tf.currDatetime(num1)
    start_date = dt.strftime(start, dstr)
    end_date =  plc.getEndDate(num2)
    
    StaPars = cparser.Parser()

    dt_start = tf.toDatetime(start_date,dstr) # datetime obj
    dt_end = tf.toDatetime(end_date,dstr)
    f_start = tf.currYearfDate(refday=dt_start) # float (fractional year)
    f_end = tf.currYearfDate(refday=dt_end)
    
    constr = plc.getConstr(sid)
    stations = plc.getStations(sid)
   
    station = stations[0]
    hytro_station = stations[1]
    
    #load the data from database
    GPS =  data.getGPS(sid, dt_end, dt_start)
    Seis = data.getDataBase(dt_start, dt_end, sid)
    GAS = data.getGasData(station, dt_start, dt_end)  
    Hytro =  data.getHytroData(hytro_station, dt_start, dt_end)

    ## create the figure
    fig = ft.spltbbfourFrame(Ylabel=None, Title=None)
    fig = ft.tsfourfigTickLabels(fig,period=(dt_end-dt_start))

    # setting upp the axis stuff
    seis_axis = fig.axes[0] 
    seis_maxes = [seis_axis, seis_axis.twinx()]
    
    gps_axis = fig.axes[1]
    gps_maxes = [gps_axis]
    
    hytro_axis = fig.axes[2]
    hytro_maxes = [hytro_axis, hytro_axis.twinx(), hytro_axis.twinx()]
    hytro_maxes[-1].spines['right'].set_position(('axes', 1.08))
    hytro_maxes[-1].set_frame_on(True)
    hytro_maxes[-1].patch.set_visible(False)

    gas_axis = fig.axes[3]
    gas_maxes = [gas_axis, gas_axis.twinx()]
    
    fig.subplots_adjust(right=0.75)
    
    # Time limits
    seis_axis.set_xlim([dt_start,dt_end])
    gps_axis.set_xlim([dt_start,dt_end])
    hytro_axis.set_xlim([dt_start,dt_end])
    gas_axis.set_xlim([dt_start,dt_end])
    
    #create the plot
    multi.plotSeis(Seis, seis_axis, seis_maxes, sid, constr)
    multi.plotGPS(GPS, gps_axis, gps_maxes, sid, constr) 
    multi.plotHytro(Hytro,hytro_axis,hytro_maxes,hytro_station)
    multi.plotGas(GAS, gas_axis, gas_maxes, station)
    
    # write to image file
    home = Path.home()
    relpath = "multiplot/figures/"
    filebase = "multiplot"
    filename = home.joinpath(relpath,filebase).as_posix()
    #filename = "/home/gpsops/multiplot/figures/multiplot"
    print("multiplot.png created")
    tplt.saveFig(filename, "png", fig)
    del fig

    
