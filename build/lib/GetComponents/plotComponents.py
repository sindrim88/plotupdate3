#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime
import pandas as pd
from datetime import datetime
from datetime import timedelta
from matplotlib.colors import LinearSegmentedColormap

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

from configparser import ConfigParser

#file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()
config.read('config.ini')

from datetime import datetime as dt
timestamp_format="%a %-d.%b %Y, %H:%M"
timestamp=dt.now()
dstr="%Y-%m-%d %H:%M:%S"
dfstr="%Y-%m-%d"  # date format



"""
Display the timestamp (green/red colorwarn) on the graph
"""
def annotateTimeUpdate(seis_maxesDepth,gps_maxes,dt_end, dt_end2,color_warn,color_warn2):

    # round the timestamp to lst second
    df = pd.DataFrame({'timestamp':[timestamp]})
    newTimestamp = df.timestamp.dt.round('1s')
    newTimestamp = newTimestamp.item() # .item() does something to allow .annotate to compile the new timestamp 

    seis_maxesDepth[1].annotate(newTimestamp, xy=(dt_end2+timedelta(0), 0), xytext=(10,250), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn2)
    
    seis_maxesDepth[1].annotate(newTimestamp, xy=(dt_end2+timedelta(0), 0), xytext=(10,700), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn2)
    
    seis_maxesDepth[0].set_ylabel("Depth [km]", fontsize=20, labelpad=4)

    gps_maxes[1].annotate(newTimestamp, xy=(dt_end+timedelta(0), 0), xytext=(10,50), textcoords='offset points', horizontalalignment='left', fontsize=20, rotation="vertical", color=color_warn)
    
    return
    
    
"""
Old and probably useless
"""
def setLandmarks(seis_axis, seis_axisLat, sid, Seis):
    landmarks = getLandmarks(sid)
    if landmarks != None:            #Testing
        print(landmarks[3])
        for i in range(0, len(landmarks)):
            print(landmarks[i])      #Testing
        """
        #Landmarks.. Will be stored with configparser later on as text
        seis_axis.text(0.01, 0.85, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axis.text(0.01, 0.420, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axis.text(0.01, 0.16, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axis.text(0.01, 0.62, "Flatey", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axisLat.text(0.01, -0.98, "Husavik", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axisLat.text(0.0051, -0.8, "Gjogurta", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        seis_axisLat.text(0.01, -0.22, "Eyjafjardarall", fontsize= 23, transform=seis_axis.transAxes,
                           bbox=dict(facecolor='orange', alpha=0.1), ha='left'  )
        """
    return



"""
set Horizontal Lines if they are described in thee config.ini file
reads them from config.ini and displays them.
"""
def setHorizLines(seis_maxes, seis_maxesLat, dt_end2, dt_start2, sid, N):
    
    #colors = ['green','blue','gold','red'] # could use this line to color code the lines along with landmarks
    landmarksLon = gethLonLandmarks(sid)
    landmarksLat = gethLatLandmarks(sid)
    
    hLonLines = gethLonLines(sid)
    hLatLines = gethLatLines(sid)
    
    if hLonLines != None:
        #Set Lon lines
        for i in range(0,len(hLonLines)):
            seis_maxes[0].hlines(y= hLonLines[i], xmin=dt_start2, xmax=dt_end2, color = 'gray', zorder=1, linewidth=1.5)
        
    #Plot Latlines if they exist
    if hLatLines != None:
        #Set Lat lines
        for i in range(0, len(hLatLines)):
            seis_maxesLat[0].hlines(y= hLatLines[i], xmin=dt_start2, xmax=dt_end2, color= 'gray', zorder=1, linewidth=1.5)
    
    return
    
    
    
    
def plotLandmarks(seis_axis, seis_axisLat, N, sid):
    
    landmarksLon = gethLonLandmarks(sid)
    landmarksLat = gethLatLandmarks(sid)
    hLonLines = gethLonLines(sid)
    hLatLines = gethLatLines(sid)
    
    end = getEndDate(N+0.05)
    strTime = dt.strftime(end, dstr)
    
    setlon = config.getfloat(sid,'min_lon')
    setLat = config.getfloat(sid,'min_lat')
   
    if hLonLines != None:
        #Set Lon lines
        for i in range (len(landmarksLon)):
            strLandmark = landmarksLon[i] 
            seis_axis.annotate(strLandmark, xy=(pd.Timestamp(strTime), setlon), xytext=(pd.Timestamp(strTime),hLonLines[i] ),fontsize = 30)
    
    if hLatLines != None:
        #Set Lat lines
        for i in range (len(landmarksLat)):
            strLandmark = landmarksLat[i] 
            seis_axisLat.annotate(strLandmark, xy=(pd.Timestamp(strTime), setLat), xytext=(pd.Timestamp(strTime),hLatLines[i] ),fontsize = 30)
        
    return




"""
Set the colors for earthquake sizes
"""
def setSeisColors(Seis):
    Seis['Mcolors'] = float('nan')
    Seis.loc[  Seis['MLW'] < 2 , 'Mcolors'] = ['green']
    Seis.loc[ ( Seis['MLW'] >= 2 ) & ( Seis['MLW'] < 3 ) , 'Mcolors'] = 'gold'
    Seis.loc[ ( Seis['MLW'] >= 3 ) & ( Seis['MLW'] < 4 ) , 'Mcolors'] = 'darkorange'
    Seis.loc[ ( Seis['MLW'] >= 4 ) & ( Seis['MLW'] < 5 ) , 'Mcolors'] = 'orangered'
    Seis.loc[ ( Seis['MLW'] >= 5 ) & ( Seis['MLW'] < 6 ) , 'Mcolors'] = 'red'
    Seis.loc[ ( Seis['MLW'] >= 6 ), 'Mcolors'] = 'darkred'
    return  Seis['Mcolors'] 
    
    

"""
Lines right before color warning, to seperate the current time and updated earthqukes 
"""
def setVerticalUpdateLines(seis_maxes, seis_maxesLat, seis_maxesDepth , gps_maxes, dt_end, dt_end2 ):
    #Time update vertical lines
    seis_maxes[0].axvline(dt_end2, color='gray')
    seis_maxesLat[0].axvline(dt_end2, color='gray')
    seis_maxesDepth[0].axvline(dt_end2, color='gray')
    gps_maxes[0].axvline(dt_end, color='gray')
    
    return


"""
Create a title for the graph, e.g:  "Graf gert: Wed 15.Jul 2020, 11:50" 
"""
def getColorWarnTitle():
    Title = "Graf gert: {}".format( timestamp.strftime(timestamp_format) )
    return Title



"""
check if the updates are on time or are at the current time
"""
def setColorWarn(dt_end):
    if (timestamp - dt_end) > timedelta(minutes=15):
        color_warn="r"
    else:
        color_warn="g"
    
    return color_warn
    
    
"""
display color coded earthqukes for different sizes on the top graph
"""
def setpatch(seis_maxes):
    #markSize = [22.5, 20, 17.5, 15, 12.5, 10] 
    patchColors = ["darkred", "red","orangered","darkorange","gold","green"]
    texts = ["6+", "5-6", "4-5", "3-4", "2-3", "0-2"]
    
    patches = [ plt.plot([],[], marker="o", ms=25,  mec='black', color=patchColors[i], 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
    
    seis_maxes[0].legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.15, 0.87), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    return



"""
get all landmarks, weather it's lon or lat
"""
def getLandmarks(sid):
    check = config.has_option(sid,'landmarks') #check if landmarks exist
    if check:
        landmarks = config.get(sid,'landmarks')
        cut = len(landmarks)-1
        cut = slice(1,cut)
        landmarks = [idx for idx in landmarks[cut].split(',')]
        
        return landmarks
    else:
        return 

    
""" 
#Get landmarks for Longitude    
"""
def gethLonLandmarks(sid):
    check = config.has_option(sid,'landmarksLon') #check if landmarks exist
    if check:
        landmarks = config.get(sid,'landmarksLon')
        cut = len(landmarks)-1
        cut = slice(1,cut)
        landmarks = [idx for idx in landmarks[cut].split(',')]
        
        return landmarks
    else:
        return 

    
"""
#Get landmarks for Latitude
"""
def gethLatLandmarks(sid):
    check = config.has_option(sid,'landmarksLat') #check if landmarks exist
    if check:
        landmarks = config.get(sid,'landmarksLat')
        cut = len(landmarks)-1
        cut = slice(1,cut)
        landmarks = [idx for idx in landmarks[cut].split(',')]
        
        return landmarks
    else:
        return 


"""
#Get Longitude coordinates 
"""
def gethLonLines(sid):
    check = config.has_option(sid,'hLonLines') #check if landmarks exist
    if check:
        hLonLines = config.get(sid,'hLonLines')
        cut = len(hLonLines)-1
        cut = slice(1,cut)
        hLonLines = [float(idx) for idx in hLonLines[cut].split(',')]
        return hLonLines
    else:
        return

    
"""   
#Get Latitude coordinates
"""
def gethLatLines(sid):
    check = config.has_option(sid,'hLatLines') #check if landmarks exist
    if check:
        hLatLines = config.get(sid,'hLatLines')
        cut = len(hLatLines)-1
        cut = slice(1,cut)
        hLatLines = [float(idx) for idx in hLatLines[cut].split(',')]
        
        return hLatLines
    else:
        return 

    

def getConstr(sid):
    min_lat = float(config.get(sid,'min_lat'))
    max_lat = float(config.get(sid,'max_lat'))
    min_lon = float(config.get(sid,'min_lon'))
    max_lon = float(config.get(sid,'max_lon'))
    constr = {
                    "min_lat" : min_lat,  "max_lat":  max_lat,
                    "min_lon":  min_lon, "max_lon":  max_lon,
    }
    return constr


def getLoc():
    loc = "IMO"
    if loc=="IMO":
        conn_dict={ 
               "dbname"    : "gas",
               "user"      : "gas_read",
               "password"  : "qwerty7",
               "host"      : "dev.psql.vedur.is",
               "port"      :  5432
               }
    elif loc=="home":
        # runn to make this work
        #ssh -f gpsops@rek.vedur.is -L 5431:dev.psql.vedur.is:5432 -N
        conn_dict={
                "dbname"    : "gas",
                "user"      : "gas_read",
                "password"  : "qwerty7",
                "host"      : "localhost",
                "port"      :  5431
                }
    return conn_dict


"""
set pend as the ending time for displaying the data on the graph 
"""
def getPend(num, dt_end, dt_start):
    if num == 0:
        pend = timestamp + (dt_end - dt_start)/40
    else:
        pend = dt_end + (dt_end - dt_start)/40
    
    return pend
        


"""
get the second time period ending date
"""
def getEndDate(timeRange2):
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    
    now = datetime.now()
    
    if timeRange2 == 0:
        end_date2=dt.strftime(dt.now(), dstr)
    else:
        end_date2 = tf.currDatetime(timeRange2)
    
    return end_date2




"""
Plots the staring time on a latitude/longitude plot, scale the time to the image so that it is always 
at the same place on the plot.
"""
def setTimeperiodOnPlot(s_str, axis, start, dt_start2, latlon):
    
    start2 = str(start)
    # Just show the first 16 characters in the string
    start2 = start2[0:16]
    
    if latlon == 0:
        #Find the right place for the time
        up = abs(config.getfloat(s_str,'min_lon'))
        down = abs(config.getfloat(s_str,'max_lon'))
        middle = (up-down)/25
        latlon = config.getfloat(s_str,'min_lon') + middle
    
    elif latlon == 1:
        #Find the right place for the time
        up = abs(config.getfloat(s_str,'min_lat'))
        down = abs(config.getfloat(s_str,'max_lat'))
        middle = (up-down)/25
        latlon = config.getfloat(s_str,'min_lat') - middle
        
    axis.annotate(start2, xy=(dt_start2, latlon), xytext=(dt_start2, latlon),fontsize = 22, color = 'green')
    return

"""
#Get coordinates from config.ini with ConfigParser
#Coordinates are stored as strings.
"""
def getCoord(s_str):
    
    #get coordinates from config.ini file, with ConfigParser.
    s_str = s_str.rsplit('2',1)[0] # cut off the end just in case the input is 2 
    
    # thn look for the correct station name i the database 
    min_lat = config.getfloat(s_str,'min_lat')
    max_lat = config.getfloat(s_str,'max_lat')
    min_lon = config.getfloat(s_str,'min_lon')
    max_lon = config.getfloat(s_str,'max_lon')
    
    coord = [min_lat, max_lat, min_lon, max_lon]
    return coord




"""
#Get the gas station names and/or hytro_station name 
#from config.ini with ConfigParser, stored as strings.
"""
def getStations(s_str):
    
    #get station names from config.ini file, with ConfigParser.
    s_str = s_str.rsplit('2',1)[0] # cut off the end just in case the input is 2 
    station = config.get(s_str,'station')
    hytro_station = config.get(s_str,'hytro_station')
    
    stations = [station, hytro_station]
    return stations


"""
Created while configuring the modules
"""
def testfunction():
    print("This function is only here for module functionality checking")
    return


"""
simple function for testing config
"""
def getLandmarknames(sid):  
    str1 = config.get(sid,'landmarks')
    print(str1)
    return 



"""
Displays a box with the landmarks and the lat/lon with the landmark names in the uuper left corner of a fig.
createLabelBox has been replaced with:

plotLandmarks(seis_axis, seis_axisLat, N, sid): 

and

setHorizLines(seis_maxes, seis_maxesLat, dt_end2, dt_start2, sid, N):
"""
def createLabelBox(seis_maxes, seis_maxesLat, dt_end2, dt_start2, sid, N):
   
    landmarksLon = gethLonLandmarks(sid)
    landmarksLat = gethLatLandmarks(sid)
    hLonLines = gethLonLines(sid)
    hLatLines = gethLatLines(sid)
    
    #Plot Lonlines if they exist
    if hLonLines != None:
        #Set Lon lines
        for i in range(0,len(hLonLines)):
            str1 = str(hLonLines[i])
            seis_maxes[0].hlines(y= hLonLines[i], xmin=dt_start2, xmax=dt_end2, color = 'gray', zorder=1,
                                    label = landmarksLon[i] + " " + str1, linewidth=1.5)
        
    #Plot Latlines if they exist
    if hLatLines != None:
        #Set Lat lines
        for i in range(0, len(hLatLines)):
            str2 =  str(hLatLines[i])
            seis_maxesLat[0].hlines(y= hLatLines[i], xmin=dt_start2, xmax=dt_end2, color= 'gray', zorder=1,
                                    label = landmarksLat[i] + " " + str2, linewidth=1.5)
    
    if hLonLines != None:
        #store legend to plot later with earthquake size legend.  
        leg1 = seis_maxes[0].legend(loc = 'upper left') # this is so we can put two legend boxes on the graph
        setpatch(seis_maxes)        # patch legend 
        seis_maxes[0].add_artist(leg1)  # Landmark legend
    else:
        setpatch(seis_maxes)
    
    if hLatLines != None:
        seis_maxesLat[0].legend(loc = 'upper left')
    
    return
    
    
    
    