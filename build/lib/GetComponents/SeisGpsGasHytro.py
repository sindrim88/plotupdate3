#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys

import numpy as np
import geofunc.geo as geo
import datetime
import pandas as pd
from datetime import datetime
from datetime import timedelta
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib

matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

from configparser import ConfigParser

#file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()
config.read('config.ini')    
#########################################################
######     functions for two-three-fourplots     ########
#########################################################
def plotSeis(Seis, seis_axis, seis_maxes, sid, constr):
    
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    ## ploting seismic
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    
    ###    laga cum_moment ####
    #Seis['cum_moment'].fillna(method='ffill', inplace=True)
    from datetime import datetime as dt
    import gtimes.timefunc as tf
    dstr="%Y-%m-%d %H:%M:%S"
    dfstr="%Y-%m-%d"  # date format
    
    Seis.dropna(inplace=True)
    
    # moment plot
    seis_maxes[0].set_zorder(1)
    seis_maxes[0].set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize='large')
    seis_maxes[0].plot(Seis.index, Seis.cum_moment/moment_scale, color="black", label='Cumulative moment', zorder=4)
    
    seis_maxes[0].fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
    
    #fill in the gap between last earthquake and the present time
    x = [Seis.index.max(), dt.now()]
    y = [Seis.cum_moment.max()/moment_scale, Seis.cum_moment.max()/moment_scale]
    seis_maxes[0].plot(x, y, color="black", label='Cumulative moment', zorder=4)
    seis_maxes[0].fill_between(x, 0, y, facecolor=(.8,.8,.8), alpha=0.4)
    
    seis_maxes[0].set_ylim(*ylim_moment/moment_scale)
    seis_maxes[0].legend(loc=(0.05,0.79), numpoints=3, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
    # stem plot
    seis_maxes[1].set_zorder(5)
    seis_maxes[1].set_ylabel('Magnitude', fontsize='large')
    
    markerline, stemlines, baseline = seis_maxes[1].stem(Seis.index, Seis.MLW,
                use_line_collection=True)
    plt.setp(stemlines, ls="-", color=(.6,.6,.6), lw=0.4, alpha=1.0)
    plt.setp(markerline, mfc='blue', mec='blue', ms=6, label='Magnitude $\mathrm{M_{LW}}$')
    plt.setp(baseline, visible=False, alpha=0.1)
    #seis_maxes[0].set_xlim(xlim[0],xlim[1])
    seis_maxes[1].set_ylim(*ylim_magnitude)
    seis_maxes[1].legend(loc=(0.04,0.66), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)
    area = [ "min_lon",  "max_lon", "min_lat",  "max_lat"]

    seis_infotext= sid + "Seismicity in the the area \n {:.2f} to {:.2f} and {:.2f} to {:.2f}".format(*[ constr[key] for key in  area ] ) 
    seis_axis.text(0.50, 0.80, seis_infotext, fontsize=15, transform=seis_axis.transAxes,
                   bbox=dict(facecolor='orange', alpha=0.1), ha='center'  )
    return



"""
"""
def plotGPS(GPS, gps_axis, gps_maxes, sid, constr):
    ## ploting GPS
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    ylim_hlength=np.array([ylsc*min(GPS.hlength), ylsc*max(GPS.hlength)])
    
    gps_maxes[0].errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes[0].set_ylabel("Displacement\n[mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.hlength, marker='o', markersize=5, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes[0].errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes[0].plot_date(GPS.index, GPS.up, marker='o', markersize=5, markerfacecolor='b', 
               markeredgecolor='b', label='Vertical displacemt')
    gps_maxes[0].legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=1, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    return




"""
"""
def plotHytro(Hytro,hytro_axis,hytro_maxes,hytro_station):

    TW1_color=(.6,.6,.6)
    TL1_color=(.8,.8,.8)
    hytro_maxes[2].set_zorder(1)
    hytro_maxes[2].patch.set_visible(False)
    hytro_maxes[2].set_ylabel("Temperature [$^{\circ}$C]", fontsize='large', labelpad=4)
    TW_plot = hytro_maxes[2].plot_date(Hytro.index, Hytro.Tw1_med_C, marker='o', markersize=1, markerfacecolor=TW1_color, 
               markeredgecolor=TW1_color, label="Water Temperature")
    TL_plot = hytro_maxes[2].plot_date(Hytro.index, Hytro.TL1_med_C, marker='o', markersize=1, markerfacecolor=TL1_color, 
               markeredgecolor=TL1_color, label="Air Temperature")

    hytro_maxes[0].set_zorder(10)
    hytro_maxes[0].patch.set_visible(False)
    hytro_maxes[0].set_ylabel("Conductivity\n[$\mu\mathrm{S\,cm^{-1}}$]", fontsize='large', labelpad=4)
    EC_plot = hytro_maxes[0].plot_date(Hytro.index, Hytro.EC_med_us, marker='o', markersize=1, markerfacecolor='r', 
               markeredgecolor='r', label="Conductivity",  alpha=1.0)
    hytro_maxes[0].set_ylim(ymin=50) #CO2_max])

    hytro_maxes[1].set_zorder(3)
    hytro_maxes[1].patch.set_visible(False)
    hytro_maxes[1].set_ylabel("Water level [m]", fontsize='large', labelpad=4)
    Wl_plot=hytro_maxes[1].plot_date(Hytro.index, Hytro.W1_med_cm/100, marker='o', markersize=1, markerfacecolor='b', 
               markeredgecolor='b', label="Water level", alpha=1.0)

#hytro_axis.legend( (EC_plot), loc=(0.20,0.67), ncol=4, numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
    hytro_maxes[2].legend(loc=(0.15,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
    hytro_maxes[0].legend(loc=(0.00,0.80), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)
    hytro_maxes[1].legend(loc=(0.00,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=5, framealpha=0.5)

    hytro_infotext="Hytrological station in Mulakvísl {}".format(hytro_station)
    hytro_axis.text(0.50, 0.9, hytro_infotext, fontsize=15, transform=hytro_axis.transAxes,
                   bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    return


 
"""
"""
def plotGas(GAS, gas_axis, gas_maxes, station):
   
    gas_maxes[0].set_zorder(1)
    #CO2_max=GAS.co2_percentage1]*100+3
    gas_maxes[0].set_ylabel("Consentration [$\%$]", fontsize='large', labelpad=4)
    gas_infotext= station
    try:
        gas_maxes[0].plot_date(GAS.index, GAS.co2_percentage, marker='o', markersize=5, markerfacecolor='black', 
                markeredgecolor='black', label="CO$_2$ [$\%$]", zorder=1)

        gas_maxes[1].set_zorder(1)
        maxgas = max(max(GAS["h2s_concentration"]) , max(GAS["so2_concentration"] ))
        #gas_maxes[1].set_ylim([-0.1,2.5*maxgas])
        gas_maxes[1].set_ylabel("Consentration\n[ppm]", fontsize='large', labelpad=4)
        gas_maxes[1].plot_date(GAS.index, GAS.h2s_concentration, marker='o', markersize=5, markerfacecolor='red', 
                   markeredgecolor='red', label='H$_2$S [ppm]',zorder=1)

        gas_maxes[1].plot_date(GAS.index, GAS.so2_concentration, marker='o', markersize=5, markerfacecolor='b', 
                  markeredgecolor='b', label='SO$_2$ [ppm]')
        gas_maxes[1].set_ylim(ymin=0, ymax=1.05*maxgas)
        #legend
        gas_maxes[1].legend(loc=(0.05,0.66), numpoints=1, frameon=True, edgecolor="white", framealpha=0.1)
        gas_maxes[0].legend(loc=(0.05,0.52), numpoints=1, frameon=True, edgecolor="white", framealpha=0.1)

        gas_axis.text(0.50, 0.9, gas_infotext, fontsize=15, transform=gas_axis.transAxes,
                      bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    except AttributeError:
        gas_axis.text(0.5, 0.5, gas_infotext+"\nNo Gas data found", fontsize=35, transform=gas_axis.transAxes,
                      bbox=dict(facecolor='orange', alpha=0.1), ha='center' )

    gas_maxes[0].set_ylim(ymin=0) #CO2_max])
    
    return