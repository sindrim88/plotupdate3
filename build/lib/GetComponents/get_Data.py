#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

from pylab import *
import numpy as np
import geofunc.geo as geo
import datetime
import pandas as pd
from datetime import datetime
from datetime import timedelta
from matplotlib.colors import LinearSegmentedColormap
import GetComponents.plotComponents as components

from matplotlib import cm
import matplotlib
matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

from configparser import ConfigParser

#file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()
config.read('config.ini')

from datetime import datetime as dt
timestamp_format="%a %-d.%b %Y, %H:%M"
timestamp=dt.now()
dstr="%Y-%m-%d %H:%M:%S"
dfstr="%Y-%m-%d"  # date format


"""
Get the data from database for seismic activity
"""
def getDataBase(dt_start, dt_end, sid):
    import geo_dataread.sil_read as gdrsil
    pre_path= "/mnt_data/"
    sil_path="sildata/"
    col_names = ['latitude', 'Dlatitude', 'longitude', 'Dlongitude', 
                    'depth', 'Ddepth', 'MLW', 'seismic_moment', 'cum_moment', 'Q']
    
    constr = components.getConstr(sid)
    Seis = gdrsil.read_sil_data(start=dt_start, end=dt_end, base_path=pre_path+sil_path,  
            fread="default", aut=True, aut_per=2, rcolumns=col_names, cum_moment=True, logger='default', **constr)
   
    return Seis


"""
Get the gps data from database for gps coordinate displacements
"""
def getGPS(sid, dt_end, dt_start):
    from gtimes.timefunc import TimetoYearf
    import geo_dataread.gps_read as gdrgps
    pre_path= "/mnt_data/"
    logging_level=logging.INFO
    ## Get GPS data with some basic filtering
    gps_path="gpsdata/"
    #-#detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2010,1,1)] # FOR HVOL
    #-#detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2014,4,1)] # FOR HVOL
    ## Get GPS data with some basic filtering
    detrend_period=[TimetoYearf(2001,1,1), TimetoYearf(2019,1,1)]
    detrend_line=[TimetoYearf(2010,6,1), TimetoYearf(2019,4,1)]
    GPS = gdrgps.read_gps_data(sid, Dir=pre_path+gps_path, start=dt_start, end=dt_end, detrend_period=None, 
                   detrend_line=None, logging_level=logging_level)

    return GPS


 
"""
"""
def getHytroData(hytro_station, dt_start, dt_end):
    import geo_dataread.hytro_read as gdrhyt
    
    pre_path= "/mnt_data/"
    logging_level=logging.INFO
    #station="lagu"
    ## Get hytrological data
    hytro_path="hytrodata/" 
    Hytro = gdrhyt.read_hytrological_data(hytro_station,start=dt_start, end=dt_end, frfile=False, fname="hytro-data.pik", 
                                 wrfile=False, base_path=pre_path+hytro_path, logging_level=logging_level )
    return Hytro



"""
"""
def getGasData(station, dt_start, dt_end):
    import geo_dataread.gas_read as gdrgas
    
    conn_dict = components.getLoc()
    device="crowcon"
    logging_level=logging.INFO
    fname = "{0:s}_{1:s}.p.gz".format(station,device)
    dsource="DBASE"
    GAS = gdrgas.read_gas_data(station, device, start=dt_start, end=dt_end, 
                             dsource=dsource, wrfile=False, fname=fname, with_excluded=True, 
                             id_observation_start=False, conn_dict=conn_dict, logging_level=logging_level)
    return GAS


