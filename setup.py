from setuptools import setup, find_packages

setup (
    name = 'multiplot',
    author='Sindri Markúuson',
    version = '3.7.6',
    description='GPS time series manipulations',
    url = 'https://gitlab.com/sindrim88/plotupdate',
    author_email='sindrima@vedur.is',
    license='Icelandic Met Office',
    packages=find_packages(),
    zip_safe=False
)