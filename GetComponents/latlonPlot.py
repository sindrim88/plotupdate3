#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function

import numpy as np
import geofunc.geo as geo
import datetime
import pandas as pd
from datetime import datetime
from datetime import timedelta
from datetime import datetime as dt

from GetComponents import plotComponents as plc
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib

matplotlib.pyplot.stem
matplotlib.axes.Axes.stem

from configparser import ConfigParser
#file = r'/home/sindrim/anaconda3/Verkefni/multiplots/configFolder/config.ini'
config = ConfigParser()

#######################################################
#####       Funtions for latlon plots         #########
#######################################################

####   Scatter Longitude earthquakes
def plotLon(Q2, Y2, seis_maxes,constr, seis_axis, dt_start2, pend2, sid):
    
    config.read(plc.sidConfigParser(sid))
    
    min_lon = float(config.get(sid,'min_lon'))
    max_lon = float(config.get(sid,'max_lon'))
    
    seis_maxes.set_zorder(0) 
    seis_maxes.set_ylim(min_lon, max_lon)
    seis_maxes.set_xlim([dt_start2, pend2])
    seis_maxes.set_ylabel("Longitude", fontsize=20, labelpad=4)
                                                                                                                
    seis_maxes.scatter(Y2.index, Y2["longitude"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxes.scatter(Q2.index, Q2["longitude"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    return
    


####   Scatter Latitude earthquakes...
def plotLat(Q2, Y2, seis_maxesLat, dt_start2, pend2, sid):
    
    config.read(plc.sidConfigParser(sid))
    min_lat = float(config.get(sid,'min_lat'))
    max_lat = float(config.get(sid,'max_lat'))
   
    seis_maxesLat.set_zorder(0)
    seis_maxesLat.set_ylim(min_lat, max_lat)
    
    seis_maxesLat.set_xlim([dt_start2, pend2])
    seis_maxesLat.set_ylabel("Latitude", fontsize=20, labelpad=4)

    seis_maxesLat.scatter(Y2.index, Y2["latitude"], alpha = 0.75, c=Y2['Mcolors'], zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesLat.scatter(Q2.index, Q2["latitude"], alpha = 0.75, c=Q2['Mcolors'], zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')

    return



def plotDepth(Q2,Y2, seis_maxesDepth, dt_start2, pend2, x1, x2):
    
    seis_maxesDepth.set_xlim([dt_start2, pend2])
    seis_maxesDepth.set_ylabel("Depth [km]", fontsize=20, labelpad=4)
    
    seis_maxesDepth.scatter(Y2.index, Y2["depth"], alpha = 0.75, c=Y2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Y2['MLW'], marker = 'o',edgecolors='face')
    
    seis_maxesDepth.scatter(Q2.index, Q2["depth"], alpha = 0.75, c=Q2['Mcolors'], label='Cumulative moment', zorder=4, s=4*3.5**Q2['MLW'], marker = 'o',edgecolors='black')
    
    seis_maxesDepth.set_ylim(x1,x2)
    
    return


#GPS displacements
def plotGps(GPS, gps_maxes,gps_axis, start, pend, sid, ratio):
    # Make the gps axis dynamic
    plc.makeGpsYaxisDynamic(GPS, gps_maxes, ratio)
    
    gps_maxes.patch.set_visible(False)
    gps_maxes.set_xlim([start, pend])
    gps_maxes.errorbar(GPS.index, GPS.hlength, yerr=GPS.Dhlength, ls='none', ecolor='grey', elinewidth=0.2, 
                     capsize=0.6, capthick=0.2)
    gps_maxes.set_ylabel("Displacement\n[mm]", fontsize=20, labelpad=4)
    gps_maxes.plot_date(GPS.index, GPS.hlength, marker='o', markersize=8, markerfacecolor='r', 
               markeredgecolor='r', label='Horizontal displacemt')

    gps_maxes.errorbar(GPS.index, GPS.up, yerr=GPS.Dup, ls='none', ecolor='grey', elinewidth=0.2, 
                         capsize=0.6, capthick=0.2)
    #gps_maxes[1].set_ylabel("Vertical\ndisplacement [mm]", fontsize='large', labelpad=4)
    gps_maxes.plot_date(GPS.index, GPS.up, marker='o', markersize=8, markerfacecolor='black', 
               markeredgecolor='black', label='Vertical displacemt')
    gps_maxes.legend(loc=(0.05,0.67), numpoints=1, frameon=True, edgecolor="white", markerscale=2, framealpha=0.5)

    gps_infotext="GPS station {}".format(sid)
    gps_axis.text(0.5, 0.9, gps_infotext, fontsize=15, transform=gps_axis.transAxes, 
                  bbox=dict(facecolor='orange', alpha=0.1), ha='center' )
    
    return




def plotColorCodeQukes(Seis, start, pend, gps_maxes):
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])
    
    # this sets the colorgradient on the earthquake markers and is just decoration at the momnt. 
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list("",["blue","orange","red"])
    
    #create a smaller dataframe for earthquakes bigger than 4.5
    theBigOnes = Seis.loc[Seis['MLW'] >= 4.5]
    
    gps_maxes.patch.set_visible(False)
    gps_maxes.set_xlim([start, pend])
    
    #Plot colorcoded earthquakes by date
    gps_maxes.scatter(Seis.index, Seis.MLW, c=Seis.index, cmap=cmap, s = 100,  marker =  "o", linewidth = 0.3, edgecolors='k')
    
    #   Bigger earthquakes are drawn twice,
    #   the lime green stars, inefficient, but they are not many.
    gps_maxes.scatter(theBigOnes.index, theBigOnes.MLW, c= 'lime', cmap=cmap, s = 2000,  marker =  "*", linewidth = 0.4, edgecolors='k')
    gps_maxes.set_ylabel("Magnitude [$\mathrm{M_{W}}$/$\mathrm{M_{L}}$]", fontsize=24, labelpad=4,)

    return



def plotStems(Seis, start, pend, gps_maxes):
    gps_maxes.set_xlim([start, pend])
    gps_maxes.stem(Seis.index,  Seis.MLW, 'gray', markerfmt=" ", use_line_collection = True)
    return



def plotMoment(Seis, start, pend, gps_maxes):
    Seis.dropna(inplace=True)
    xlsc=1.02 # x-axis limits w.r.t. data
    ylsc=1.05 # y-axis limits w.r.t. data (max.abs)
    moment_scale=1e14
    ylim_magnitude=np.array([0, ylsc*max(Seis.MLW)])
    ylim_moment=np.array([0, ylsc*max(Seis.cum_moment)])

    gps_maxes.plot(Seis.index, Seis.cum_moment/moment_scale, color="black", zorder=4)
    gps_maxes.fill_between(Seis.index, 0, Seis.cum_moment/moment_scale, facecolor=(.8,.8,.8), alpha=0.4)
    
    #fill in the gap between last earthquake and the present time
    x = [Seis.index.max(), dt.now()]
    y = [Seis.cum_moment.max()/moment_scale, Seis.cum_moment.max()/moment_scale]
    gps_maxes.plot(x, y, color="black", label='Cumulative moment', zorder=4)
    gps_maxes.fill_between(x, 0, y, facecolor=(.8,.8,.8), alpha=0.4)
    gps_maxes.set_ylim(*ylim_moment/moment_scale*1)
    
    gps_maxes.patch.set_visible(False)
    gps_maxes.set_xlim([start, pend])
    gps_maxes.set_ylabel("Seismic moment\n [$10^{14}\,$Nm]", fontsize=24, labelpad=4)

    texts = ["Moment magnitude [$\mathrm{M_{W}}$] for reviewed events) / Local magnitude [$\mathrm{M_{L}}$]  for not reviewed events"]
    
    patches = [ plt.plot([],[], marker="o", ms=15,  mec='black', color='gray', 
            label="{:s}".format(texts[i]))[0]  for i in range(len(texts)) ]
   
    gps_maxes.legend(facecolor=None, framealpha=0, handles=patches,bbox_to_anchor=(0.215, 0.90), 
           loc='center', ncol=2, numpoints=1 ,fontsize = 20)
    return
    
