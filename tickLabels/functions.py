from datetime import timedelta
from gtimes.timefunc import currTime
from matplotlib import figure
from datetime import date
from pandas.tseries import offsets
from datetime import datetime

import matplotlib.pyplot as plt
import timesmatplt.timesmatplt as tplt
import timesmatplt.gasmatplt as gplt
import matplotlib.ticker as mticker
import matplotlib.dates as mdates

import matplotlib as mpl
import pandas as pd
import numpy as np
import time

"""
spltbbthreeFrame and tstwofigTickLabels create three figures instead of four. 
"""
def fourFrame(Ylabel=None, Title=None, depth = None, figNum=None):
    """
        Ylabel, 
        Title,

    output:
        fig, Figure object.

    """
    #import matplotlib.image as image
    mpl.style.use('classic')
    #from matplotlib import gridspec
    # constructing a figure with three axes and adding a title
    multibl=1.3
    
    #gridspec sets the y axis scale for the three different subplots(latlon figures)
    if depth != None:
        fig, axes = plt.subplots(nrows=4, ncols=1,figsize=(20*multibl,23*multibl), gridspec_kw={'height_ratios': [23*multibl,23*multibl, 11.5*multibl, 23*multibl]} )
        plt.rc('font', size=20)
        #print("this is strange")
    else:
        multibl=1.3
        fig, axes = plt.subplots(nrows=figNum, ncols=1, figsize=(20*multibl,23*multibl))
        fig.subplots_adjust(hspace=0.1)
         
    if type(Title) is list:
        plt.suptitle(Title[0], y=0.865,x=0.651)
        axes[0].set_title(Title[1],y=1.01)
    elif type(Title) is str:
        axes[0].set_title(Title)
    else:
        pass

    if Ylabel == None: #
        Ylabels = ("Y1","Y2","Y3","Y4")
    
    # --- apply custom stuff to the whole fig ---
    plt.minorticks_on
    plt.gcf().autofmt_xdate(rotation=0, ha='left')
    mpl.rcParams['legend.handlelength'] = 0    
    mpl.rcParams['text.usetex'] = True
    mpl.rc('text.latex', preamble=r'\usepackage{color}')
    
    for i in range(figNum):  
        axes[i].set_ylabel(Ylabel, fontsize=16, labelpad=0)
        axes[i].axhline(0,color='black') # zero line

    for ax in axes[-1:-5:-1]:
    #tickmarks lables and other and other axis stuff on each axes 
    # needs improvement
    
    # --- X axis ---
        xax = ax.get_xaxis()
        xax.set_tick_params(which='minor', direction='in', length=4, top='off')
        xax.set_tick_params(which='major', direction='in', length=10, top='off')
        if ax is axes[0]: # special case of top axes
            xax.set_tick_params(which='minor', direction='in', length=4, top='off')
            xax.set_tick_params(which='major', direction='in', length=10, top='off')

    return fig


def tstwofigTickLabelsFourFrame(fig,period=None, period2 = None, figNum = None, Depth = None):
    
    if Depth != None:
        per = period2.days 
    axes = fig.axes
    
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off', labelsize=20) 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10, labelsize=20) 
    
    if figNum == 4 and Depth == None:
        four(axes,period)
        
    elif figNum == 3:
         three(axes, period, period2)
        
    elif figNum == 2:
        print("two")
        two(axes,period)
        
    elif Depth == True:
        lat(axes, period, period2, per)
    
    return fig



def four(axes, period):
    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
        # --- X axis ---
        xax = ax.get_xaxis()
        xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
        xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
        xax = tplt.tslabels(xax,period=period,locator='major',formater=None)

        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')

        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)

        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)

    return



def three(axes, period, period2):
    count = 0
    for ax in axes[-1:-5:-1]:
        
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
        
        # --- X axis ---
        xax = ax.get_xaxis()
        if ax is axes[0]:
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period2,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period2,locator='major',formater=None)
        else:
            xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
            xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
            xax = tplt.tslabels(xax,period=period,locator='major',formater=None)
        
        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
                
        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)
       
        count+= 1
    return

def two(axes, period):
    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
    
        # --- X axis ---
        xax = ax.get_xaxis()
        xax = tplt.tslabels(xax,period=period,locator='minor',formater='minor')
        xax = tplt.tslabels(xax,period=period,locator='minor',formater=None)
        xax = tplt.tslabels(xax,period=period,locator='major',formater=None)

        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')

        xax.set_tick_params(which='minor', direction='inout', length=4)
        xax.set_tick_params(which='major', direction='inout', length=15)

        for tick in xax.get_major_ticks():
            tick.label1.set_horizontalalignment('center')

        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10)

    return




def lat(axes, period, priod2, per):
  
    # major labels in separate layer
    axes[-1].get_xaxis().set_tick_params(which='major', labelbottom='off', labelsize=20) 
    axes[-1].get_xaxis().set_tick_params(which='major', pad=10, labelsize=20) 
    
    for ax in axes[-1:-5:-1]:
        #tickmarks lables and other and other axis stuff on each axes 
        # needs improvement
        ax.grid(True)
        ax.grid(True,linestyle='solid',axis='x')
        if period < timedelta(2600):
            if ax is not axes[-1]:
                ax.grid(True, which='minor',axis='x',)
                ax.grid(False, which='major',axis='x',)
        # --- X axis ---
        xax = ax.get_xaxis()
        ### This handles the possible different time periods for the graphs.
        ### first graph has a different period than th other two
        
        if ax is axes[-1]:
            xax = tplt.tslabels(xax,period=period,locator='major',formater='major')
            xax = tplt.tslabels(xax,period=period,locator='major',formater='minor')
            
        if per < 10 and ax != axes[-1]:
            
            minorLoc = mpl.dates.HourLocator(byhour=[0,8,16])
            minorFmt = mpl.dates.DateFormatter('%d %H:%M')
            majorLoc = mpl.dates.DayLocator()
            majorFmt = mpl.dates.DateFormatter('%d %b %y')
            xax.set_minor_locator(minorLoc)
            xax.set_major_locator(majorLoc)
            xax.set_minor_formatter(minorFmt)
            xax.set_major_formatter(majorFmt)
            ax.set_xticks(np.linspace(ax.get_xticks(), ax.get_xticks(), len(ax.get_xticks())))

        elif per < 20 and ax != axes[-1]:
            minorLoc = mpl.dates.HourLocator(byhour=[0])
            minorFmt = mpl.dates.DateFormatter('%b %d')
            majorLoc = mpl.dates.DayLocator()
            majorFmt = mpl.dates.DateFormatter('%d %b %y')
            xax.set_minor_locator(minorLoc)
            xax.set_major_locator(majorLoc)
            xax.set_minor_formatter(minorFmt)
            xax.set_major_formatter(majorFmt)
            ax.set_xticks(np.linspace(ax.get_xticks(), ax.get_xticks(), len(ax.get_xticks())))
      
        else:
            minorLoc = mpl.dates.AutoDateLocator()
            minorFmt = mpl.dates.DateFormatter('%b %d')
            majorLoc = mpl.dates.MonthLocator(interval=1)
            majorFmt = mpl.dates.DateFormatter('%b %Y')
            xax.set_minor_locator(minorLoc)
            xax.set_major_locator(majorLoc)
            xax.set_minor_formatter(minorFmt)
            xax.set_major_formatter(majorFmt)
            ax.set_xticks(np.linspace(ax.get_xticks(), ax.get_xticks(), len(ax.get_xticks())))
      
            
        xax.set_tick_params(which='minor', direction='inout', length=4, labelsize=20)
        # --- Y axis ---
        yax = ax.get_yaxis()
        yax.set_minor_locator(mpl.ticker.AutoMinorLocator())
        yax.set_tick_params(which='minor', direction='in', length=4)
        yax.set_tick_params(which='major', direction='in', length=10,labelsize = 17)
        #yax.yticks(fontsize=30)
    return 

